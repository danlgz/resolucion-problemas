import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-numeros-rectangulares',
  templateUrl: './numeros-rectangulares.component.html',
  styleUrls: ['./numeros-rectangulares.component.css']
})
export class NumerosRectangularesComponent implements OnInit {
  catetoA: number;
  catetoB: number;
  hipotenusa: number;

  constructor() { }

  ngOnInit() {
    this.calcular();
  }

  calcular() {
    loop1:
    for (let x = 200; x <= 600; x++) {
      for (let y = 200; y <= 600; y++) {
        const hipotenusa = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        if ((x + y + hipotenusa) === 1000) {
          this.catetoA = x;
          this.catetoB = y;
          this.hipotenusa = hipotenusa;
          break loop1;
        }
      }
    }
  }

  /**
   * Sabiendo que en un triangulo rectángulo la hipotenusa es mayor que los catetos,
   * tomé como premisa que la hipotenusa tendía que ser mayor a 400 y los catetos
   * estarían entre 0 y 600 ya que la media de este intervalo es 300.
   * 300 < 400, esto cumple con que los catetos sean menores que la hipotenusa.
   * El segundo análisis fue que el valor de los catetos estarían en el intervalo [200, 600]
   * ya que la suma de estos debe ser mayor a la hipotenusa, sacando un valor máximo
   * de los catetos menores a 200 serían: catetoA = 199 y catetoB = 199; catetoA + catetoB = 398
   * y esto es menor que el valor mínimo de la hipotenusa (400) por tanto sería falso
   * sacando los valores mínimos de [200, 600] sería: catetoA = 200 y catetoB = 200; catetoA + catetoB = 400
   * a partir de esto se deduce que los dos catetos deben ser mayores a 200 o bien
   * uno sea 200 y el otro mayor a 200, ya que si ambos son 200 serían igales a la hipotenusa,
   * por tano no sería un tríanguo rectángulo
   */

}
