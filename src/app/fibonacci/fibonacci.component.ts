import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fibonacci',
  templateUrl: './fibonacci.component.html',
  styleUrls: ['./fibonacci.component.scss']
})
export class FibonacciComponent implements OnInit {
  content = [];
  width: string;
  ancho = 20;
  alto = 100;
  numero: number;
  error: string;

  constructor() { }

  ngOnInit() {
  }

  fibonacci() {
    if (this.numero === 0 || !this.numero) {
      this.content = [];
      this.error = '';
      return;
    } else if (this.numero < 0) {
      this.error = 'Por favor, escribe un número positivo';
      return;
    }
    this.content = [];
    const nuevoNumero = this.numero - 1;
    let anterior = 0;
    let posterior = 1;
    let resultado: number;

    this.content.push(posterior);

    if (nuevoNumero > 0) {
      for (let i = 0; i < nuevoNumero; i++) {
        resultado = anterior + posterior;
        anterior = posterior;
        posterior = resultado;
        this.content.push(resultado);
      }
    }

    this.width = `${100 / (this.content.length)}%`;
  }

  displayHeight(numero: number) {
    return `calc(${this.calcHeight(numero)}vh - 17em)`;
  }

  calcHeight(height: number): number {
    return +((height * 100) / (this.content[this.content.length - 1])).toFixed(2);
  }

}
