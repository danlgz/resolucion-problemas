import { Routes } from '@angular/router';
import { NumerosRectangularesComponent } from './numeros-rectangulares/numeros-rectangulares.component';
import { FibonacciComponent } from './fibonacci/fibonacci.component';

export const ROUTES: Routes = [
  { path: '', redirectTo: 'rectangulares', pathMatch: 'full' },
  { path: 'rectangulares',  component: NumerosRectangularesComponent },
  { path: 'fibonacci',      component: FibonacciComponent             },
];
