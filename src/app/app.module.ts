import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { NumerosRectangularesComponent } from './numeros-rectangulares/numeros-rectangulares.component';
import { FibonacciComponent } from './fibonacci/fibonacci.component';
import { ROUTES } from './app.routes';


@NgModule({
  declarations: [
    AppComponent,
    NumerosRectangularesComponent,
    FibonacciComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
